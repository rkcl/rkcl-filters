/**
 * @file butterworth_low_pass_filter.h
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Define a Butterworth low pass filter wrapped in RKCL using DSPFilters (https://github.com/vinniefalco/DSPFilters)
 * @date 31-01-2020
 * License: CeCILL
 */

#pragma once

#include <rkcl/core.h>
#include <DspFilters/Dsp.h>

/**
  * @brief Namespace for everything related to RKCL
  *
  */
namespace rkcl
{

/**
 * @brief Utility class to apply a Butterworth low pass filter on data
 *
 */
class ButterworthLowPassFilter : public Callable<ButterworthLowPassFilter>
{
public:
    /**
	 * @brief Return true if the template typename Derived inherits from Eigen::MatrixBase
	 * @tparam A template matrix-based typename
	 */
    template <typename Derived>
    using is_matrix_expression = typename std::is_base_of<Eigen::MatrixBase<std::decay_t<Derived>>, std::decay_t<Derived>>;

    /**
	 * @brief Construct a new Butterworth Low Pass Filter with the given parameters
	 * @param order filter order
	 * @param sampling_frequency data sampling frequency
	 * @param cutoff_frequency filter cutoff frequency
	 */
    ButterworthLowPassFilter(int order, double sampling_frequency, double cutoff_frequency);

    /**
	 * @brief Construct a new Butterworth Low Pass Filter object using a YAML configuration file
	 * Accepted values are: 'order','sampling_frequency' and 'cutoff_frequency'
	 * @param configuration The YAML node containing the filter parameters
	 */
    ButterworthLowPassFilter(const YAML::Node& configuration);

    /**
	 * @brief Defaut destructor.
	 */
    ~ButterworthLowPassFilter() = default;

    /**
	 * @brief Filter any Eigen::Matrix
	 * @param data the matrix
	 */

    template <typename T>
    typename std::enable_if<is_matrix_expression<T>::value, void>::type
    setData(T& data)
    {
        setData(data.data(), data.rows() * data.cols());
    }

    /**
	 * @brief Filter any vector of double values.
	 * @param  data the pointer to the first element of the vector
	 * @param  size the number of elements in the vector
	 */
    void setData(double* data, size_t size);

    /**
	 * @brief Ensure that the stored filter parameters are consistent
	 */
    void checkParams();

    /**
	 * @brief Initialize the filter using the stored parameters
	 * @return true on success, false otherwise
	 */
    bool init();
    /**
	 * @brief Execute the filtering process on the data and replace the original values
	 * with the filtered ones
	 * @return true if the filtering process succeeded, false otherwise
	 */
    bool process();

private:
    int order_;                     //!< the filter order
    double sampling_frequency_;     //!< the data sampling frequency
    double cutoff_frequency_;       //!< the filter cutoff frequency
    std::vector<double**> channels; //!< the channels used by the filter (one channel for each variable)
    size_t size_;                   //!< the number of variables to filter

    std::vector<Dsp::SimpleFilter<Dsp::Butterworth::LowPass<5>, 1>> filters_; //!< A vector of DSP Butterworth low pass filters (one for each variable) with max order 5 and 1 channel

    bool is_data_set_; //!< Indicate if the data has already been given to the channels
};
using ButterworthLowPassFilterPtr = std::shared_ptr<ButterworthLowPassFilter>;
} // namespace rkcl
