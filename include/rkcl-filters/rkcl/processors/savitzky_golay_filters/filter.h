/**
 * @file filter.h
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Define a RKCL utility class to perform Savitzky-Golay filtering based on Gram polynomials
 * @date 31-01-2020
 * License: CeCILL
 */

#pragma once

#include <rkcl/core.h>

namespace rkcl
{

/**
 * @brief Base class for Savitzky-Golay filters common data and functions
 *
 */
class SavitzkyGolayFilter : public Callable<SavitzkyGolayFilter>
{
public:
    /**
	 * @brief Construct a new Savitzky Golay Filter with the provided parameters
	 *
	 * @param m  Window size is 2*m+1
	 * @param t  Initial Point Smoothing (i.e evaluate polynomial at first point in the window). Points are defined in range [-m;m]
	 * @param n Polynomial Order
	 * @param s Derivate? 0: no derivation, 1: first derivative...
	 * @param dt Samples time step
	 */
    SavitzkyGolayFilter(int m, int t, int n, int s, double dt);

    /**
	 * @brief Construct a new Savitzky Golay Filter object using a YAML configuration file
	 * Accepter values are: 'm', 't', 'n', 's' and 'dt'
	 * @param configuration The YAML node containing the filter parameters
	 */
    SavitzkyGolayFilter(const YAML::Node& configuration);

    /**
	 * @brief Defaut destructor.
	 */
    ~SavitzkyGolayFilter() = default;

    /**
	 * @brief Pure virtual function to initialize the filter
	 * @return true on success, false otherwise
	 */
    virtual bool init() = 0;
    /**
	 * @brief Pure virtual function to execute the filtering
	 * @return true on success, false otherwise
	 */
    virtual bool process() = 0;

protected:
    int m_;     //!<Window size is 2*m+1
    int t_;     //!<Initial Point Smoothing (i.e evaluate polynomial at first point in the window). Points are defined in range [-m;m]
    int n_;     //!<Polynomial Order
    int s_;     //!<Derivate? 0: no derivation, 1: first derivative...
    double dt_; //!<Samples time step
};
using SavitzkyGolayFilterPtr = std::shared_ptr<SavitzkyGolayFilter>;
using SavitzkyGolayFilterConstPtr = std::shared_ptr<const SavitzkyGolayFilter>;
} // namespace rkcl
