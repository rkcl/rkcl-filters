 #include <rkcl/processors/savitzky_golay_filters/savitzky_golay_filters.h>
 #include <rkcl/processors/data_logger.h>
 #include <pid/rpath.h>

 #include <iostream>
 #include <vector>



int main()
{
  int m = 10, t = 10, n = 1, s = 0;
  double dt = 1;
  rkcl::SavitzkyGolayMatrixFilter sg_filter(m, t, n, s, dt);

  Eigen::Vector3d vec, vec_filtred;
  vec.setZero();
  vec_filtred.setZero();

  vec(0) = 1;
  vec_filtred(0) = 1;

  sg_filter.setInputData(vec_filtred);

  sg_filter.init();

  rkcl::DataLogger data_logger(PID_PATH("sg_log"));
  data_logger.log("vec_unfiltred", vec);
  data_logger.log("vec_filtred", vec_filtred);

  data_logger.process(0);



  for (size_t i=1; i < 100; ++i)
  {
    vec(0) *= -1;
    vec(1) += 2;
    vec(2) += 3;

    vec_filtred(0) *= -1;
    vec_filtred(1) += 2;
    vec_filtred(2) += 3;

    sg_filter();

    data_logger.process(dt*i);
  }

  std::cout << "Ending the application" << std::endl;

  return 0;
}
