declare_PID_Component(
    EXAMPLE_APPLICATION
    NAME sg-example
    DIRECTORY sg_example
    RUNTIME_RESOURCES sg_log
    DEPEND
        rkcl-filters
        pid-rpath/rpathlib
        rkcl-core/rkcl-utils
)
